var express = require("express"),
    request = require("request"),
    bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.set('views', __dirname+'/views')
app.set('view engine', 'jade');

var port = 3000;

var spotUrl  = "http://api-spotplatform.milanotrader.com/Api";
var username = "soso_traffic";
var password = "54e0cf2a5738b";
var countryData = [];

function getCountry(cb){
    form_data = {
        api_username: username,
        api_password: password,
        MODULE: 'Country',
        COMMAND: 'view',
        jsonResponse: 'true'
    }

    request.post({url: spotUrl, form: form_data}, function (err, resp, body) {
        var json = JSON.parse(body);
        var country = json.status.Country;
        var parsedArray = [];

        for (member in country){
            var value = [];
            value.push(country[member].id);
            value.push(country[member].iso);
            value.push(country[member].name);
            parsedArray.push(value);

        }
        cb(parsedArray)
    })

}

getCountry(function(data){
    countryData = data;
});


app.get('/',function(req, res) {
   res.render('index',{data:countryData})
});

app.get('/save',function(req, res){
    form_data = {
        api_username: username,
        api_password: password,
        MODULE: 'Customer',
        COMMAND: 'add',
        jsonResponse: 'true',
        campaignId: 3,
        sendEmail: 1,
        regulateStatus: 'none',
        regulateType: 1
    }

    if (req.query.email){
        form_data.email = req.query.email
    }
    if(req.query.firstName){
        form_data.FirstName = req.query.firstName;
    }
    if(req.query.lastName){
        form_data.LastName = req.query.lastName;
    }
    if(req.query.password){
        form_data.password = req.query.password;
    }
    if(req.query.phone){
        form_data.Phone = req.query.phone;
    }
    if(req.query.country){
        var country = req.query.country.toUpperCase();
        countryData.forEach(function(data){
            if(country == data[1]){
                form_data.Country = data[0];
            }
        })
    }
    if(req.query.currency){
        var currency = req.query.currency.toUpperCase();
        form_data.currency = currency;
    }
    if(req.query.regulateStatus){
        form_data.regulateStatus = req.query.regulateStatus;
    }
    if(req.query.regulateType){
        form_data.regulateType = req.query.regulateType;
    }


    request.post({url:spotUrl, form:form_data},function(err, resp, body){
        var json = JSON.parse(body);
        if(json.status.errors){
            res.render('error',{error:json.status.errors.error});
        }
        if (json.status.Customer){
            res.render('save',{email:form_data.email, firstName:form_data.FirstName, lastName:form_data.LastName, currency:form_data.currency, password: form_data.password, phone:form_data.Phone, country: form_data.country});
        }

    })

})

app.use('/error',function(req, res){
    res.render('error');
    res.end();
})


app.listen(port,function(){
    console.log("Server started on port: "+port)
});